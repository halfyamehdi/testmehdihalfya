export const state = () => ({
  dataclimatique: "e",
});

export const getters = {
  getdataclimatique(state) {
    return state.dataclimatique;
  },
};
export const mutations = {
  setdataclimatique(state, data) {
    state.dataclimatique = data;
  },
};

export const actions = {
  async listclimatique({ commit, state }, payload) {
    try {
      let reg = await this.$axios.$get("/api/climatique");
      return reg;
    } catch (error) {
      console.log(error);
      return error;
    }
  },
  async ajouteclimatique({ commit, state }, payload) {
    try {
      let reg = await this.$axios.$post("/api/climatique", payload);
      return reg;
    } catch (error) {
      console.log(error);
      return error;
    }
  },

  async deleteclimatique({ commit, state }, id) {
    this.$axios
      .$delete(`/api/climatique/${id}`)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        reject(error);
      });
  },
};
